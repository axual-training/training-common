package io.axual.training.common.producer;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProduceCallback implements Callback {
    private static final Logger logger = LoggerFactory.getLogger(ProduceCallback.class);

    @Override
    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
        if (e != null) {
            logger.error("Exception sending record", e);
        } else {
            logger.info("Successfully produced record with key size {} and value size {} to topic {}", recordMetadata.serializedKeySize(), recordMetadata.serializedValueSize(), recordMetadata.topic());
        }
    }
}
