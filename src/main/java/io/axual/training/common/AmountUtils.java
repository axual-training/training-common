package io.axual.training.common;

import io.axual.general.Decimal;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * Class to handle conversion from/to Amount objects to BigDecimals
 */
public final class AmountUtils {

    private static final Locale DEFAULT_LOCALE = new Locale("NL", "nl");
    private static final Currency DEFAULT_CURRENCY = Currency.getInstance("EUR");

    private AmountUtils() {
    }

    /**
     * Creates a BigDecimal object from an amount
     */
    public static BigDecimal getBigDecimal(Decimal amount) {
        return new BigDecimal(amount.getValue());
    }

    /**
     * Create Amount object with a BigDecimal, gets the scale from the BigDecimal
     */
    public static Decimal createAmount(BigDecimal bigDecimal) {
        return Decimal.newBuilder()
                .setValue(bigDecimal.toString())
                .build();
    }

    /**
     * Method to do easy readable comparisons on BigDecimals
     *
     * @param x first argument
     * @param y second argument
     * @return the result
     */
    public static boolean greaterThanOrEquals(BigDecimal x, BigDecimal y) {
        return x.compareTo(y) != -1;
    }

    /**
     * Method to do easy readable comparisons on BigDecimals
     *
     * @param x first argument
     * @param y second argument
     * @return the result
     */
    public static boolean smallerThan(BigDecimal x, BigDecimal y) {
        return !greaterThanOrEquals(x, y);
    }

    /**
     * Gets a formatted string, not displaying the currency symbol, and using the default locale
     *
     * @param amount an amount object
     * @return a formatted string
     */
    public static String getFormattedAmount(Decimal amount, String currencyString) {
        return getFormattedAmount(amount, currencyString, false);
    }

    /**
     * Gets a formatted string, using the default locale
     *
     * @param amount        an amount object
     * @param includeSymbol whether the currency symbol should be included
     * @return a formatted string
     */
    public static String getFormattedAmount(Decimal amount, String currencyString, boolean includeSymbol) {
        return getFormattedAmount(amount, currencyString, includeSymbol, DEFAULT_LOCALE);
    }

    /**
     * Gets a formatted string, using the currency to set the correct digits
     */
    public static String getFormattedAmount(Decimal amount, String currencyString, boolean includeSymbol, Locale locale) {
        Currency currency = Currency.getInstance(currencyString);
        if (currency == null) {
            currency = DEFAULT_CURRENCY;
        }
        NumberFormat format = NumberFormat.getInstance(locale);
        format.setMaximumFractionDigits(currency.getDefaultFractionDigits());
        format.setMinimumFractionDigits(currency.getDefaultFractionDigits());
        format.setGroupingUsed(true);
        String value = format.format(new BigDecimal(amount.getValue()).doubleValue());
        if (includeSymbol) {
            value = currency.getSymbol(locale) + " " + value;
        }
        return value;
    }
}
